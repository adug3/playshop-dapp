var SimpleStorage = artifacts.require("./SimpleStorage.sol");
var ShopCoin = artifacts.require("./ShopCoin.sol");

module.exports = function(deployer) {
  deployer.deploy(SimpleStorage);
  deployer.deploy(ShopCoin);
};
