pragma solidity ^0.4.18;


contract TestCoin {
    struct SellInfo {
        uint price;
        address seller;
    }

    mapping (address => uint) balances;
    mapping (address => uint) weiArray;
    SellInfo[] sellInfos;

    event Transfer(address indexed _from, address indexed _to, uint256 _value);

    function TestCoin() public {
        balances[msg.sender] = 0;
    }

    function purchase(uint amount) public {
        balances[msg.sender] += amount;
    }

    function sendCoin(address receiver, uint amount) public returns(bool sufficient) {
        if (balances[msg.sender] < amount) return false;
        balances[msg.sender] -= amount;
        balances[receiver] += amount;
        Transfer(msg.sender, receiver, amount);
        return true;
    }

    function sellItem(uint price) public returns(bool success) {
        if (balances[msg.sender] < 1) {
            return false;
        }
        sellInfos.push(SellInfo(price, msg.sender));
        balances[msg.sender] -= 1;
        return true;
    }

    function buyItem(uint sellId) public payable returns (bool) {
        SellInfo sellInfo = sellInfos[sellId];
        if (sellInfo.price > 0 && msg.value >= sellInfo.price) {
            uint lastIndex = sellInfos.length - 1;
            uint overpaid = msg.value - sellInfo.price;
            weiArray[sellInfo.seller] += sellInfo.price;
            weiArray[msg.sender] += overpaid;
            
            sellInfos[sellId] = SellInfo(sellInfos[lastIndex].price, sellInfos[lastIndex].seller);
            delete sellInfos[lastIndex];
            balances[msg.sender] += 1;
            return true;
        }
        return false;

    }

    function withdraw() public returns (bool) {
        uint amount = weiArray[msg.sender];
        if (amount > 0) {
            weiArray[msg.sender] = 0;
            if (!msg.sender.send(amount)) {
                weiArray[msg.sender] = amount;
                return false;
            }
        }
        return true;
    }

    function getBalance(address addr) public view returns(uint) {
        return balances[addr];
    }

    function getWei(address addr) public view returns(uint) {
        return weiArray[addr];
    }

    function getSellItem(uint sellId) public view returns(address seller, uint price) {
        seller = sellInfos[sellId].seller;
        price = sellInfos[sellId].price;
    }

    function getSellCount() public constant returns(uint count) {
        return sellInfos.length;
    }
}