pragma solidity ^0.4.18;


contract ShopCoinBase {
    function name() constant returns (string){
        return "Playshop Token";
    }
    
    function symbol() constant returns (string){
        return "PST";
    }
    
    uint256 internal totalSupply = 0;
    function getTotalSupply() constant returns (uint256 supply){
        return totalSupply;
    }

    mapping(address => uint) internal balances;
    function balanceOf(address _owner) constant returns (uint balance) {
        return balances[_owner];
    }
    
    //ownership
    mapping(uint256 => address) internal tokenOwners;
    mapping(uint256 => bool) internal tokenExists;

    function ownerOf(uint256 _tokenId) constant returns (address owner) {
        require(tokenExists[_tokenId]);
        return tokenOwners[_tokenId];
    }

    function tokenOfOwnerFromId(address _owner, uint256 _fromId) constant returns (uint){
        for (uint i = _fromId; i <= totalSupply; i++) {
            if (tokenOwners[i] == _owner)
                return i;
        }
        return 0;
    }
    
    //MetaData
    struct TokenMetadata{
        uint gene;
        uint rarity;
    }

    mapping(uint256 => TokenMetadata) tokenMetadataArray;
    function getTokenMetadata(uint256 _tokenId) constant returns (uint gene, uint rarity) {
        var metadata = tokenMetadataArray[_tokenId];
        return (metadata.gene, metadata.rarity);
    }
}

contract Randomness {
    uint private randomnessPool = 0;
    uint private maxPool = 97;
    
    function randomGet(uint max) public constant returns (uint, uint) {
        uint blockTimestamp = block.timestamp;
        uint mod = (blockTimestamp + randomnessPool * randomnessPool) % max;
        return (blockTimestamp, mod);
    }

    function addRandomness(uint randomFactor) public {
        randomnessPool = (randomnessPool + randomFactor * randomFactor + 1) % maxPool;
    }
}

contract ShopCoin is ShopCoinBase, Randomness {
    uint gachaPrice = 0.01 ether;
    mapping (address => uint) withdrawArray;
    
    function playGacha() public payable returns(uint){
        require(msg.value >= gachaPrice);
        uint overPaid = msg.value - gachaPrice;
        address buyer = msg.sender;
        withdrawArray[buyer] += overPaid;
        
        var (, tokenGene) = randomGet(47);
        addRandomness(tokenGene);

        uint latestTokenId = totalSupply + 1;
        tokenMetadataArray[latestTokenId] = TokenMetadata(tokenGene, tokenGene % 3);
        balances[buyer] += 1;
        totalSupply += 1;
        tokenExists[latestTokenId] = true;
        tokenOwners[latestTokenId] = buyer;
        
        //addToTokenList(buyer, latestTokenId);
        return latestTokenId;
    }
    
    struct Order {
        uint tokenId;
        uint price;
        address seller;
        uint sellTime;
        address buyer;
        uint buyTime;
    }
    
    Order[] orderList;
    mapping (uint => bool) tokenOnSale;
    function sellToken(uint tokenId, uint price) public returns(bool){
        require(tokenExists[tokenId]);
        require(!tokenOnSale[tokenId]);
        require(tokenOwners[tokenId] == msg.sender);
        
        orderList.push(Order(tokenId, price, msg.sender, block.timestamp, 0, 0));
        tokenOnSale[tokenId] = true;
        return true;
    }
    
    function buyToken(uint orderId) public payable returns(bool) {
        Order order = orderList[orderId];
        require(order.buyer == 0);
        require(msg.value >= order.price);
        require(tokenOnSale[order.tokenId]);
        
        uint overPaid = msg.value - order.price;
        address buyer = msg.sender;
        orderList[orderId].buyer = buyer;
        orderList[orderId].buyTime = block.timestamp;

        withdrawArray[buyer] += overPaid;
        withdrawArray[order.seller] += order.price;
        balances[buyer] += 1;
        balances[order.seller] -= 1;
        tokenOwners[order.tokenId] = buyer;
        tokenOnSale[order.tokenId] = false;

        //removeFromTokenList(order.seller, order.tokenId);
        //addToTokenList(buyer, order.tokenId);

    }

    function withdraw() public returns (bool) {
        uint amount = withdrawArray[msg.sender];
        if (amount > 0) {
            withdrawArray[msg.sender] = 0;
            if (!msg.sender.send(amount)) {
                withdrawArray[msg.sender] = amount;
                return false;
            }
        }
        return true;
    }

    function getWithdraw(address addr) public view returns(uint) {
        return withdrawArray[addr];
    }

    function getOrderDetail(uint orderId) public view returns(uint tokenId, address seller, uint price, address buyer) {
        tokenId = orderList[orderId].tokenId;
        seller = orderList[orderId].seller;
        price = orderList[orderId].price;
        buyer = orderList[orderId].buyer;
    }

    function getOrderCount() public view returns(uint count) {
        return orderList.length;
    }
}