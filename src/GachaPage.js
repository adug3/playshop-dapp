import React from 'react'

class Gacha extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            needUpdate: 0,
            web3: props.web3,
            coinBalance: "0",
            account: ""
        }

        this.playGacha = this.playGacha.bind(this);
    }
    componentWillMount() {
        console.log("componentWillMount")
        console.log(this.props)
        if (this.state.web3) this.updateUserInfo(this.props)
    }

    shouldComponentUpdate(nextProps, nextState){
        //console.log("shouldComponentUpdate")
        //console.log(nextProps)
        //console.log(nextState)

        return nextState.needUpdate;
    }

    componentDidUpdate(prevProps, prevState){
        this.setState({
            "needUpdate": 0
        })
    }

    componentWillReceiveProps(nextProps){
        console.log("componentWillReceiveProps")
        console.log(nextProps);


        this.updateUserInfo(nextProps)
        //if (this.state.status)
            //this.updateUserInfo(nextProps.web3);
        //console.log(this.state.web3);
    }
    updateUserInfo(nextProps){
        // Get accounts.
        //const web3 = nextProps.web3
        const shopCoin = nextProps.tokenContract
        const account = nextProps.account
        let testCoinInstance

        shopCoin.deployed().then((instance) => {
            testCoinInstance = instance
                // Get the value from the contract to prove it worked.
            return testCoinInstance.balanceOf.call(account)
        }).then((result) => {
            // Update state with the result.
            return this.setState({
                "coinBalance": result.toString(),
                "account": account,
                "tokenContract": shopCoin,
                "needUpdate": 1
            })
        })
    }

    playGacha(){
        // Declaring this for later so we can chain functions on SimpleStorage.
        const shopCoin = this.state.tokenContract;
        const account = this.state.account;
        let shopCoinInstance

        shopCoin.deployed().then((instance) => {
            shopCoinInstance = instance
            // Stores a given value, 1 by default.
            const gachaWei = this.state.web3.toWei(0.01, this.state.priceUnit)
            return shopCoinInstance.playGacha({from: account, value: gachaWei})
        }).then((result) => {
            // Get the value from the contract to prove it worked.
            return shopCoinInstance.balanceOf.call(account)
        }).then((result) => {
            // Update state with the result.
            console.log(result);
            return this.setState({
                coinBalance: result.c[0],
                needUpdate: 1
            })
        })
    }

    render(){
        return (
            <div className="Gacha">
                <p>Gacha <button onClick={this.playGacha} >Play</button></p>
                
                <table>
                    <tbody>
                        <tr><td>Item count:</td><td>{this.state.coinBalance}</td></tr>
                        <tr><td>Update:</td><td>{this.state.needUpdate}</td></tr>
                    </tbody>

                </table>
            </div>
        )
    }
}

export default Gacha