import React from 'react'
import TestCoinContract from '../build/contracts/TestCoin.json'

class Market extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            status: -1,
            web3: props.web3,
            sellCount: 0,
            viewSellId: 0,
            sellPrice: "",
            priceUnit: "ether",
            viewSellItem:{
                price: "",
                seller: "N/A"
            }
        }
        this.changeSellId = this.changeSellId.bind(this);
        this.changePrice = this.changePrice.bind(this);
        this.sellButton = this.sellButton.bind(this);
        this.buyButton = this.buyButton.bind(this);
    }

    componentWillMount() {
        this.updateSellInfo(this.state.web3)
    }

    componentWillReceiveProps(nextProps){
        this.updateSellInfo(nextProps.web3);
    }

    updateSellInfo(web3){
        if (web3){
            this.setState({
                "status": 2
            })
            const contract = require('truffle-contract')
            const testCoin = contract(TestCoinContract)
            testCoin.setProvider(web3.currentProvider)
            var testCoinInstance

            web3.eth.getAccounts((error, accounts) => {
                testCoin.deployed().then((instance) => {
                    testCoinInstance = instance
                    // Get the value from the contract to prove it worked.
                    return testCoinInstance.getSellCount.call()
                }).then((result) => { 
                    this.setState({ sellCount: result.c[0] })
                    return this.updateViewSellItem(testCoinInstance, this.state.viewSellId)
                }).then(([address, price]) =>{
                    console.log(`sell item: ${address}, ${price}`);
                    this.setState({
                        viewSellItem:{
                            "price": web3.fromWei(price, this.state.priceUnit).toString(),
                            "seller": address
                        }
                    })
                })
            });
        }
    }

    updateViewSellItem(testCoinInstance, sellId){
        if (sellId >= 0){
            return testCoinInstance.getSellItem.call(sellId)
        }else{
            return new Promise(resolve => {
                resolve([-1, {c: [0]}]);
            })
        }
    }

    sellItem(web3){
        if (web3){
            this.setState({
                "status": 2
            })
            const contract = require('truffle-contract')
            const testCoin = contract(TestCoinContract)
            testCoin.setProvider(web3.currentProvider)
            var testCoinInstance

            web3.eth.getAccounts((error, accounts) => {

                testCoin.deployed().then((instance) => {
                    testCoinInstance = instance
                    // Get the value from the contract to prove it worked.
                    const weiPrice = this.state.web3.toWei(this.state.price, this.state.priceUnit)
                    console.log(weiPrice);
                    return testCoinInstance.sellItem(weiPrice, {from: accounts[0]})
                }).then((result) => {
                    // Update state with the result.
                    if(result){
                        alert("success");
                    }else {
                        alert("item not enough")
                    }
                })
            })
        }else{
            this.setState({
                "status": 1
            });
            alert("loading");
        }
    }

    buyItem(web3){
        if (web3){
            this.setState({
                "status": 2
            })
            const contract = require('truffle-contract')
            const testCoin = contract(TestCoinContract)
            testCoin.setProvider(web3.currentProvider)
            var testCoinInstance

            web3.eth.getAccounts((error, accounts) => {

                testCoin.deployed().then((instance) => {
                    testCoinInstance = instance
                    // Get the value from the contract to prove it worked.
                    const weiPrice = web3.toWei(this.state.viewSellItem.price, this.state.priceUnit)
                    console.log(weiPrice);
                    return testCoinInstance.buyItem.sendTransaction(this.state.viewSellId, 
                        {from: accounts[0], value: weiPrice},
                        //function(error, result){}
                    )
                    //alert('hhahahah')
                    //return web3.eth.sendTransaction({from: accounts[0], to:"0x9A46034674Cc6E07e771f12D62A6093A120c95D3", value: web3.toWei(0.05, "ether"), gas:1000000})
                }).then((result) => {
                    // Update state with the result.
                    if(result){
                        alert("success");
                    }else {
                        alert("item not enough")
                    }
                })
            })
        }else{
            this.setState({
                "status": 1
            });
            alert("loading");
        }
    }

    sellButton(event){
        event.preventDefault();
        this.sellItem(this.state.web3);
    }

    buyButton(event){
        event.preventDefault();
        this.buyItem(this.state.web3);
    }

    changePrice(event){
        this.setState({price: event.target.value});
    }

    changeSellId(event){
        this.setState({viewSellId: event.target.value});
        this.updateSellInfo(this.state.web3);

    }
    render(){
        return (
            <div className="Market">
                <p>Market</p>
                <p>SellCount: {this.state.sellCount} </p>
                <form onSubmit={this.buyButton}>
                    <label>SellID: </label>
                    <input type="number" name="sellId" min="0" max={this.state.sellCount} value={this.state.viewSellId} onChange={this.changeSellId}></input>
                    <button type="submit">Buy</button>
                </form>
                <p>Price: {this.state.viewSellItem.price} ETH - Seller: {this.state.viewSellItem.seller}</p>
                <form onSubmit={this.sellButton}>
                    <label>Sell price: </label>
                    <input type="number" name="price" min="0" step="0.0001" value={this.state.price} onChange={this.changePrice}></input>
                    <button type="submit">Sell</button>
                </form>
            </div>
        )
    }
}

export default Market