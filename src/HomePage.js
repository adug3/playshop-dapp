import React from 'react'
import Constant from './Utility'


class ItemTable extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items: props.items
        }
    }

    render(){
        let rows = this.state.items.map((item) => {
            let imgStr = (Number(item[1]) + 10).toString();
            var temp = imgStr;
            let p = temp.length;
            while (p < 4){
                temp = "0" + temp;
                p = temp.length;
            }
            let imgSrc = `http://img.o.geishatokyo.com/images/m/goods_${temp}.gif`;
            return (
                <tr key={item[0].toString()}>
                    <td>{item[0]}</td>
                    <td>{item[1]}</td>
                    <td>{item[2]}</td>
                    <td><img src={imgSrc}></img></td>
                </tr>
            );

        });
        return (
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Gene</th>
                        <th>Rarity</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        )
    }
}


class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            needUpdate: 0,
            account: props.account,
            tokenContract: props.tokenContract,
            web3: props.web3,
            balance: "0",
            coinBalance: 0,
            itemList: []
        }
    }

    //for debug
    componentWillMount() {
        console.log("componentWillMount")
        console.log(this.props)
        if (this.state.web3) this.updateUserInfo(this.props)
    }

    shouldComponentUpdate(nextProps, nextState){
        //console.log("shouldComponentUpdate")
        //console.log(nextProps)
        console.log(nextState)

        return nextState.needUpdate;
    }

    componentWillUpdate(nextProps, nextState){
        //console.log("componentWillUpdate")
        //console.log(nextProps)
        //console.log(nextState)
    }

    componentDidUpdate(prevProps, prevState){
        this.setState({
            "needUpdate": 0
        })
    }

    componentWillReceiveProps(nextProps){
        console.log("componentWillReceiveProps")
        console.log(nextProps);


        this.updateUserInfo(nextProps)
        //if (this.state.status)
            //this.updateUserInfo(nextProps.web3);
        //console.log(this.state.web3);
    }

    updateUserInfo(nextProps){
        // Get accounts.
        console.log("updateUserInfo")
        console.log(nextProps);
        const shopCoin = nextProps.tokenContract
        const account = nextProps.account
        const web3 = nextProps.web3
        web3.eth.getBalance(account, 'latest', (error, balance) =>{
            return this.setState({
                "balance": web3.fromWei(balance, Constant.priceUnit).toString()
            })
        })
        console.log("get balance")
        let shopCoinInstance
        shopCoin.deployed().then((instance) => {
            shopCoinInstance = instance
            // Get the value from the contract to prove it worked.
            return shopCoinInstance.balanceOf.call(account)
        }).then((result) => {
            // Update state with the result.
            //console.log(result.c[0]);
            this.setState({
                coinBalance: result.c[0],
                "account": account,
                "tokenContract": shopCoin,
                "web3": web3,
                "needUpdate": 1
            });
            this.updateItemList(shopCoinInstance, 0, account);
        })
    }

    updateItemList(shopCoinInstance, fromId, account){
        let self = this;
        let itemList = this.state.itemList;
        let _updateItemList = function(fromId, callback){
            let tokenId
            const getItemPromise = shopCoinInstance.tokenOfOwnerFromId.call(account, fromId)
            getItemPromise.then((result) => {
                if (result.c[0]){
                    //TODO: use tail
                    tokenId = result;
                    _updateItemList(result.c[0] + 1, callback)
                    return shopCoinInstance.getTokenMetadata.call(tokenId)
                }
                else
                    return new Promise(resolve => {
                        resolve([0, 0])
                    })
            }).then(([gene, rarity]) => {
                if (tokenId){
                    itemList.push([tokenId.toString(), gene.toString(), rarity.toString()])
                }else{
                    callback();
                }
            })
        }
        _updateItemList(fromId, () => {
            self.setState({
                "itemList": itemList,
                "needUpdate": 1
            })
        });

    }

    render(){
        return (
            <div className="Home">
                <h3>Basic Information</h3>
                <div>
                    <table>
                        <tbody>
                            <tr><td>Account:</td><td>{this.state.account}</td></tr>
                            <tr><td>ETH:</td><td>{this.state.balance}</td></tr>
                            <tr><td>Item count:</td><td>{this.state.coinBalance}</td></tr>
                            <tr><td>Update:</td><td>{this.state.needUpdate}</td></tr>
                        </tbody>
                    </table>
                </div>

                <h3>Item List</h3>
                <div>
                    <ItemTable items={this.state.itemList} />
                </div>
            </div>
        )
    }
}


export default Home