import React, { Component } from 'react'
import getWeb3 from './utils/getWeb3'
//import TestCoinContract from '../build/contracts/TestCoin.json'
import ShopCoinContract from '../build/contracts/ShopCoin.json'

import {
  Route,
  NavLink,
  BrowserRouter
} from "react-router-dom";
import Home from "./HomePage";
import Gacha from "./GachaPage";
import Market from "./MarketPage";

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      web3: null,
      account: "",
      tokenContract: null,
      status: -1
    }
  }

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3.then(results => {
      const web3 = results.web3
      const contract = require('truffle-contract')
      const shopCoin = contract(ShopCoinContract)
      shopCoin.setProvider(results.web3.currentProvider)
      web3.eth.getAccounts((error, accounts) => {
        this.setState({
          "account": accounts[0],
          "web3": web3,
          "tokenContract": shopCoin,
          "status": 0
        });      
      })
    }).catch(() => {
      console.log('Error finding web3.')
    })
  }

  render() {
    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal">
          <a href="#" className="pure-menu-heading pure-menu-link">Truffle Box</a>
        </nav>

        <main className="container">
          <p>this is frame.</p>
          <BrowserRouter>
            <div>
              <h1>Simple SPA</h1>
              <ul className="header">
                <li><NavLink to="/">Home</NavLink></li>
                <li><NavLink to="/gacha">Gacha</NavLink></li>
                <li><NavLink to="/market">Market</NavLink></li>
              </ul>
              
              <p>this is frame, too</p>
              <div className="content">
                <Route exact path="/" render={() => (<Home {...this.state}/>)}/>
                <Route path="/gacha" render={() => (<Gacha {...this.state}/>)}/>
                <Route path="/market" render={() => (<Market {...this.state}/>)}/>
              </div>
            </div>
          </BrowserRouter>
        </main>
      </div>
    );
  }
}

export default App
